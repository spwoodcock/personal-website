export default {
  title: "Sam Woodcock",
  url: "https://spwoodcock.dev/",
  language: "en",
  description: "My personal site and portfolio.",
  author: {
    name: "Sam Woodcock",
    email: "sam.woodcock@live.co.uk",
    url: "https://spwoodcock.dev/about/",
  },
};
