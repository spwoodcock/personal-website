---js
const eleventyNavigation = {
	key: "Contact",
	order: 5
};
---

There are several ways that you can reach me.

You can also get my digital card in your terminal by running:

`curl -sL spwoodcock.dev/card`

## Email

[sam.woodcock@live.co.uk](mailto:sam.woodcock@live.co.uk)

## Signal

`spwoodcock.01`
