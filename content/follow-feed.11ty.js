export default class {
	data() {
		return {
			permalink: "/follow.rss"
		}
	}

	async render() {
		const { ActivityFeed } = await import("@11ty/eleventy-activity-feed");

		let feed = new ActivityFeed();

		feed.setCacheDuration("4h");

		// Blog
		feed.addSource("atom", "Blog", "https://spwoodcock.dev/feed.xml");

		// OpenStreetMap
		feed.addSource("rss", "OpenStreetMap Diary", "https://www.openstreetmap.org/user/spwoodcock/diary/rss");

		return feed.toRssFeed({
			title: "spwoodcock's Activity Feed",
			language: "en",
			url: "https://spwoodcock.dev/follow",
			subtitle: "Centralised RSS for content published by spwoodcock.",
		});
	}
};
