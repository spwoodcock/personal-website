---
title: "How to Migrate Plex from Windows to Linux: What Most Guides Miss"
date: 2025-02-09
tags: ["tutorial", "server", "media", "plex"]
description: "Migrating a home server between operating systems comes with caveats"
---

## Home Server Overhaul

- I am in the process of migrating from a hybrid Windows / HyperV Ubuntu home server setup, to a Debian-based Kubernetes cluster.
- The only app remaining on Windows was Plex, due to perceived difficult migrating all the metadata to Linux.
- All of the guides I found online had partial information, but not everything I needed to get a fully migrated Plex server up and running on my Linux machine!

> Note that I was migrating from a plain Windows install to a containerised Linux install, but for this guide, this info should not matter.

### The Official Guide

- This has most of everything you need: https://support.plex.tv/articles/201370363-move-an-install-to-another-system/
- However, do note that it's a guide explicitly for migrating to a new machine **using the same operating system**.
- There are two extra steps I will detail below, that should be carried out in the 'Copy Server Data From the Source System' section:
  - Updating paths from Windows to Linux in the Plex SQLite database.
  - Copying preferences from Windows Registry to Linux XML file.
- After following the extra steps listed here, you should have an exact replica of the Plex installation you were migrating from!

### Update SQLite File Paths

1. First backup your SQLite database for Plex, located here:

```
C:\Users\YOURUSER\AppData\Local\Plex Media Server\Plug-in Support\Databases\com.plexapp.plugins.library.db"
```

2. Enter the Plex SQLite shell

```powershell
"C:\Program Files\Plex\Plex Media Server\Plex SQLite.exe" "C:\Users\YOURUSER\AppData\Local\Plex Media Server\Plug-in Support\Databases\com.plexapp.plugins.library.db"
```

> Note that Plex uses a custom implementation of SQLite... standard browsers might corrupt your file, or just not work.

3. Replace Windows backslash with Linux forward slash

```sql
UPDATE media_parts SET file=REPLACE(file, '\', '/');
UPDATE section_locations SET root_path=REPLACE(root_path, '\', '/');
UPDATE media_streams SET url=REPLACE(url, '\', '/') WHERE url LIKE 'file://%';
```

4. Replace library root paths

```sql
// First get all of your library root locations
SELECT root_path FROM section_locations;

// For each location, replace with the correct path
UPDATE section_locations SET root_path=REPLACE(root_path, 'D:/Path/To/Library1', '/data/library1');
UPDATE section_locations SET root_path=REPLACE(root_path, 'D:/Path/To/Library2', '/data/library2');
```

5. Replace media part paths

```sql
// Inspect the format of the file paths
SELECT file FROM media_parts WHERE file != "" LIMIT 20;

// For each library above, ensure you correctly set the path here
UPDATE media_parts SET file=REPLACE(file, 'D:/Path/To/Library1', '/data/library1');
UPDATE media_parts SET file=REPLACE(file, 'D:/Path/To/Library2', '/data/library2');
```

6. Replace media stream paths

```sql
// Note here we possibly have double and triple slash prefixes that must be replaced
UPDATE media_streams SET url=REPLACE(url, 'file://D:/Path/To/Library1', 'file:///data/library1');
UPDATE media_streams SET url=REPLACE(url, 'file:///D:/Path/To/Library1', 'file:///data/library1');
UPDATE media_streams SET url=REPLACE(url, 'file://D:/Path/To/Library2', 'file:///data/library2');
UPDATE media_streams SET url=REPLACE(url, 'file:///D:/Path/To/Library2', 'file:///data/library2');
```

6. Replace metadata_item paths

```sql
UPDATE metadata_items SET guid=REPLACE(guid, 'file:///D:/Path/To/Library1', 'file:///data/library1');
UPDATE metadata_items SET guid=REPLACE(guid, 'file:///D:/Path/To/Library2', 'file:///data/library2');
```

7. Final checks

```sql
// Check for any corruption
PRAGMA integrity_check;
// If errors, try a reindex
REINDEX;

// Vacuum db
VACUUM;

// Exit
.quit
```

### Migrating Preferences

#### Windows Registry

- Windows keeps it Plex preferences in the registry.
- To access the values, open tool **regedit**
- Navigate to path: `Computer\HKEY_CURRENT_USER\SOFTWARE\Plex, Inc.\Plex Media Server`
- Inspect the values to migrate. Some important ones are:
  - `PlexOnlineXXX` values (for the PlexPass account)
  - `XXXMachineIdentifer` values
  - `customConnections` if used, say for a reverse proxy config
  - `allowedNetworks` if used
- Additional details can be found here: https://support.plex.tv/articles/201105343-advanced-hidden-server-settings/

#### Linux Preferences

- Linux places all of these values into a `Preferences.xml` file.
- First, start up and shut down your new Plex instance.
- The `Preferences.xml` file will be created in the 'Plex Media Server' root directory.
- Modify the preferences with the values from the Windows registry:

```xml
<?xml version="1.0" encoding="utf-8"?>
<Preferences
  allowedNetworks="127.0.0.1,192.168.90.11"
  autoEmptyTrash="0"
  secureConnections="1"
  sendCrashReports="1"
  customConnections="https:/your.custom.domain:443"
  AcceptedEULA="1" 
  AnonymousMachineIdentifier="xxx-xxx"
  CertificateUUID="xxx-xxx"
  CertificateVersion="3"
  CloudSyncNeedsUpdate="0"
  DatabaseCacheSize="4000"
  DisableTLSv1_0="1"
  DlnaEnabled="0"
  DvrIncrementalEpgLoader="0"
  EnableIPv6="0"
  FSEventLibraryPartialScanEnabled="0"
  FSEventLibraryUpdatesEnabled="0"
  FriendlyName="Name-Of-Your-Plex-Server"
  GlobalMusicVideoPathMigrated="1"
  LanNetworksBandwidth="192.168.90.11/24"
  LanguageInCloud="1"
  LastAutomaticMappedPort="0"
  LogVerbose="0"
  MachineIdentifier="xxx-xxx"
  ManualPortMappingMode="1"
  ManualPortMappingPort="32400"
  MergedRecentlyAdded="0"
  MetricsEpoch="1"
  MinutesAllowedPaused="30"
  OldestPreviousVersion="legacy"
  PlexOnlineHome="0"
  PlexOnlineMail="your@email.com"
  PlexOnlineToken="token_from_previous_install"
  PlexOnlineUsername="your_plex_username"
  PreferredNetworkInterface="eth0"
  ProcessedMachineIdentifier="xxx-xxx"
  PubSubServer="xxx"
  PubSubServerPing="xx"
  PubSubServerRegion="lhr"
  PublishServerOnPlexOnlineKey="1"
  PushNotificationsEnabled="1"
  RelayEnabled="0"
  ScannerLowPriority="1"
  ScheduledLibraryUpdateInterval="86400"
  ScheduledLibraryUpdatesEnabled="0"
  TranscodeCountLimit="3"
  TranscoderCanOnlyRemuxVideo="0"
  TranscoderH264BackgroundPreset="medium"
  TranscoderQuality="0"
  TranscoderTempDirectory="/tmp/transcode"
  TranscoderThrottleBuffer="1800"
  WanPerUserStreamCount="2"
  WanTotalMaxUploadRate="20000"
  WebHooksEnabled="0"
/>
```
