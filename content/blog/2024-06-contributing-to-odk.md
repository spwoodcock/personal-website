---
title: "Contributing to ODK: the go-to mobile form data collection tool"
date: 2024-06-13
tags: ["opensource", "data", "community"]
description: "Excellent open-source tool ecosystems should be supported"
---

This is a post with very little code, but instead a higher level retrospective on contribution to a mature open-source project.

## Intro to ODK

[ODK](https://getodk.org) is a collection of open-source tools that are used to collect data via forms designed for a mobile phone.

It is at the forefront of tools available for this purpose, commonly used in sectors such as public health, global development, crisis response, environmental research, and more.

The key tools to be aware of are:

- **ODK Collect**: the Android application written in Java/Kotlin, used to collect data.
- **ODK Central**: the Node.js web server written in JavaScript, used to receive the data from mobile devices. There is a companion web frontend written in Vue.

## Additional requirements from field mappers

At the Humanitarian OpenStreetMap team (HOT), we use ODK to underpin one of our core tools, the Field Mapping Tasking Manager ([FMTM](https://github.com/hotosm/fmtm)).

Mappers in the field must use ODK Collect to gather information about features on the ground (buildings, roads, etc).

However, there are two major features that would significantly improve the workflow for mappers:

1. To have an easy way to load base satellite imagery in the background of ODK Collect.

2. To open ODK Collect from the click of a button in FMTM, with information pre-filled, such as a selected building.

Luckily, the ODK community and developers, are both very welcoming, and also very receptive to feedback and new ideas.

### Implementing easier MBTile usage

ODK has supported loading basemaps in MBTile format for a while now, however adding them to a project requires connecting a computer and manually copying the files over.

After fruitful [discussion](https://forum.getodk.org/t/provide-a-way-to-get-mbtiles-to-collect-without-having-to-connect-to-a-computer/42206) around this task on the ODK forums, a plan was made by the ODK devs to improve the implementation.

HOT works closely with [NAXA](https://naxa.com.np) for development on FMTM, and NAXA's current Chief Operating Officer is incidentally one of their most senior Android devs!

Nishon managed to put in a big shift to implement most of the requirements for loading the MBTiles file directly from a device [here](https://github.com/getodk/collect/pull/5917).

Unfortunately due to lack of capacity from our side, the final touches to the PR were not able to be made 😅 Grzegorz from the ODK team picked up the feature and finalised it for their most recent beta release (2024.2.0-beta.2).

However, overall it was a very successful collaboration & we are extremely grateful to the ODK team for being so responsive and receptive to contribution. 

### Loading ODK Collect by intent

More recently we have been focusing on our second requirement: loading ODK Collect from an external app, with data pre-filled.

We started [discussion](https://forum.getodk.org/t/launch-odkcollect-from-a-browser-with-feature-pre-selected/43190/8) on this a while ago, gathering community input and requirements.

Ping, an very experienced dev who used to be part of ODK, helped us to create a [proof of concept](https://github.com/hotosm/odkcollect/pull/2). After further bug fixing by Samir at NAXA, we finally have a built dev APK with all of our requirements. 

Obviously this proof of concept would not pass ODK's very strict code quality requirements, including extensive testing and covering a well researched combination of user requirements.

However, we plan to continue the discussion and development (primarily by Samir) for hopefully another successful collaboration with the ODK team.

### Other contributions

During this process I have also been providing additional contributions to documentation and build configs (as my experience does not lie in Android development), to help build goodwill and aid the long term success of ODK.

At the organisation level, and individual level, we are very keen to see the suite of ODK tools grow and the team to continue the exemplary job they are doing managing an open-source project!

Some minor contributions:
- Helping debug the ODK Central deployment on ARM-based systems.
- Upgrading the Python version for pyxform-http.
- Tweaking the containerised deployment of Central.
- Proof reading and adding content to docs.
- Testing all the tools in various scenarios.
- Early adoption of new features and providing feedback.

## Retrospective

Contributing to an established application with many complex user requirements is no easy task.

Thankfully the developers at ODK are extremely professional about this process, and while expect a very thorough and professional contribution, they are open to assist and provide guidance throughout the entire process.

It may take some extra time to gather user requirements, provide comprehensive tests, and review the code more thoroughly than normally accustomed, however in the end it has been very rewarding. ODK is, and remains to be, the most reliable and complete suite of tools for form-based data collection, and hopefully with the support of the open-source community will continue to be long into the future!
