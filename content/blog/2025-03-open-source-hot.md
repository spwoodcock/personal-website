---
title: "Building an open-source community at HOTOSM: a thank you"
date: 2025-03-10
tags: ["community", "opensource"]
description: "The open-source humanitarian software community is relatively small but welcoming"
---

I'm deeply embedded in the open-source humanitarian software space by now.

This post is a retrospective of my time spent here so far, and a highlight of the successes from the open-source community at HOTOSM.

I apologise in advance if its overly long and bit rambly.

## Where This All Began

I started working with HOTOSM as a volunteer in Feb 2022. They needed urgent assistance with developing a new tool called the Field Mapping Tasking Manager, in response to the Turkey / Syria earthquake crisis at the time.

![fmtm-call-to-action](/images/blog/2025-03-open-source-hot-fmtm-call.jpg)

Having worked on open-source tools for the preceding two years, I was pretty keen to volunteer my time.

After speaking with their senior humanitarian advisor (and now a great friend), [Ivan Gayton](https://ivangayton.net),
I was quickly convinced about the potential for the tool, and took a week off work to push forward developments.

Fast forward a few years and I am now the senior technical lead at HOTOSM, coordinating and assisting the development of
our entire [suite of tools](https://www.hotosm.org/tech-suite.html).

> [!NOTE] Governance
> HOTOSM mostly operates on a no-nonsense do-ocracy model.
> Whoever contributes the most has the most sway over a project!
>
> Of course, this doesn't always hold true, particularly when hiring contractors
> to assist with the development of our tools.
>
> But HOT does try to be extremely transparent with it's approach, working in the
> open, and actively encouraging input from the community.

## The Community

Community has always been HOT's strongest asset 🤗

I am by no means the expert on this topic, but I do know that the community largely formed around
[Tasking Manager](https://tasks.hotosm.org), responding to humanitarian activations for disaster response and resiliance.

Sub-communities formed around procuring and processing base imagery, in-person mapathon events globally,
country-wide communities deeply rooted in OpenStreetMap and other open communities such as OSGeo and YouthMappers.

The global reach of the community is truly astounding, with ~5000 users registered on
[Slack channels](https://slack.hotosm.org) alone.

HOT also has a large network of software development collaborators, with names such as DevelopmentSeed
and Kontur frequently stepping in to assist.

While our mapping and data volunteer network is strong, we still struggle to cultivate a sustainable
open-source software development community. Why is that? Would a different governance model help?

> [!NOTE]
> HOT may be joining the Cloud Native Geo Foundation in the near future,
> a development that I am particularly excited to see!

## Software Development Skills Are Truly Global

A degree from a top university doesn’t automatically make someone a great developer.
Real-world problem-solving, especially in humanitarian tech, demands a broader skill set — one that
prioritizes practicality over algorithmic challenges.

Sure you can produce exceptionally crafted algorithms, but are the end users needs best served by this?
Having rarely lived through hardship or a humanitarian crisis, is our mindset suitably aligned to meet
the multi-faceted requirements? (Of course, I realize the irony here — this probably exposes my own biases too).
This is especially prescient in an often resource-constrained humanitarian sector.

I have worked with a mixed bag of developers, many of which don't immediately grasp the bigger picture
of what they are developing.
When funneling $$$ into AWS isn't always an option, thinking outside the box may be required.
Sometimes simple (and low resource, low budget) is best.

It also often requires understanding the full stack implications of what you are developing: delivering tools from end to end, considering development time, ease of understanding and maintainability, plus hosting costs in the long term (factoring possible scaling too).

Where I am going with this is that the top coding talent need not be constrained to the talent pool from software
engineer grads from the "Western world". Hiring for projects in this sector has been an eye opening experience.
The rest of the world is getting on fine with some excellent software developers. You just need to know where to look.

Case in point to this is the burgeoning geospatial software industry based in Kathmandu, Nepal.
One of the most talented and 'going-somewhere' engineers I have met is my friend and colleague
[Kshitij Sharma](https://www.hotosm.org/updates/staff-spotlight-series-kshitij-sharma), who graduated
from the excellent Institute of Engineering at Tribhuvan University.

HOT's biggest tech partner is the organisation [NAXA](https://naxa.com.np), an organisation
that is truly a pioneer of the geospatial development industry, working alongside many, many
international partners to achieve theirs goals.

From this partnership, [DroneTM](https://dronetm.org) was conceived, a tool for community
driven drone imagery collection - with the vast majority of the technical challenges being
solved by the team based in Nepal.

## Notable Contributions

Since working with HOT, I have had some excellent community contribution experiences regarding software development.

Here are a couple of examples.

### HOTOSM UI

- [Joe](https://github.com/joltcode) has been invaluable to the team in the past few years.
- Not only did he hash out the design choices taken for the [hotosm/ui](https://github.com/hotosm/ui) project,
  a web component library that will be integrated into all of our tools into the future.
- But Joe has also contributed many **large** patches to both Tasking Manager and FieldTM, overhauling our
  build tools, migrating the entire Tasking Manager codebase from JavaScript --> TypeScript, and more 🙌

### Simple Reverse Geocoding

- [Emir](https://github.com/emirfabio) worked on this fantastic self-contained project over a few months,
  with the outcome being a very professional and comprehensive solution.
- The goal was simple: a basic [reverse-geocoder based on PostGIS](https://github.com/hotosm/pg-nearest-city),
  that required no external dependencies.
- The design pattern used for sync/async Python code can be carried across to future projects too!
- Emir is currently working on the final extension of the project: internationalised city name output.

### GeoJSON AOI Parsing

- **Luke**, a volunteer who recently reached out, is working on a small idea I had a while ago called
  [geojosn-aoi-parser](https://github.com/hotosm/geojson-aoi-parser).
- Pretty much all of our tools do some sort of user-provided geojson parsing.
- The plan with this module is to standardise the parsing to a consistent format, while
  again only relying on PostGIS as a dependency (all of our tools use this).

### FMTM Test Cases

- [Azhar](https://github.com/azharcodeit) was an [Outreachy](https://www.outreachy.org) intern who helped
  with creating many test cases for the FieldTM backend.
- This has paid dividends for the stability of the API into the future.
- She also assisted with the DroneTM flight plan generation module, adding support for arbitrary rotation.

### Others

- **Charlie**, who assisted the brainstorming for a new data conflation workflow in FieldTM (which we haven't managed
  to see to completion yet, sorry!)
- **Ozgur**, who has been assisting us with the development of a PowerBI dashboard for managers of field data collection campaigns.
- On top of these notable contributions I have been present for, there have been many more (particularly for Tasking Manager),
  which I haven't mentioned: productive discussions, filing bug reports, making PRs - it all counts!
- All of the brainstorming and collaboration we have done with the ODK team and community to date! This was touched on in
  [another blog post](https://spwoodcock.dev/blog/2024-06-contributing-to-odk)

## Call To Action

With a funding crisis in the humanitarian sector currently - from COVID, to global instability, to the downright
unethical closing of USAID - the purse strings are continually tightening.

We need your contributions more than ever!
Find our how you can best contribute [here](https://docs.hotosm.org/become-a-contributor)

Feel free to reach out to member of the Tech Team through any channel.
Let's grow HOT's humanitarian tech community one new contributor at a time 💪
