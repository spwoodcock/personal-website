---js
const eleventyNavigation = {
	key: "About",
	order: 3
};
---
# About

Hi, I'm Sam!

If you wish to get in touch, head on over to the [contact page](/contact) and you can use any of the options listed there.

## Projects

- [Website](https://spwoodcock.dev): This site! ([source code](https://gitlab.com/spwoodcock/personal-website))
- [HOTOSM's E2E Mapping Tools](https://github.com/hotosm): Senior tech lead and key software architect for end-to-end community mapping solutions.
- [Bayanat Human Rights Monitoring](https://github.com/sjacorg/bayanat): Contract to assist with the development of an open source data management solution for human rights documentation.
- [MastWeb](https://gitlab.com/MastWeb): Sole developer for full stack citizen science project, collecting data on tree 'masting' incidence across Switzerland.
- [EnviDat](https://github.com/EnviDat): Assisting with the development of an environmental research data portal.
- [Viridien Atmospheric Gas Montitoring](https://satelliteblog.cgg.com/space-based-observations-of-atmospheric-trace-gases-part-1/): Co-developed a tool to monitor global atmospheric trace gasses such as NO² using Senintel 5P satellite data cubes.
- [TerraTools](https://gitlab.com/spwoodcock/terratools): QGIS plugin for raster image manipulation and easier registration of historical geological maps.
- [Natural Wonders Map](https://gitlab.com/spwoodcock/natural-wonders-map): Android application to view natural wonders of the world on a map, searchable with curated data and informative notes.

## Professional

- Developer specialised in containerisation and geospatial.

- Primarily focused on web application development.

- Experienced in the establishment and maintenance software covering the full stack.

- Looking to enhance my skillset through collaborative learning and maximise social capital.

- Currently working with Python, TypeScript, Golang.

## Education

- (2:1) BSc Geology @ Imperial College London

- (1st) MSc Mining Geology @ University of Exeter 

## Hobbies

- Cycling, hiking, film.

- Championing humanitarian causes.

- PC hardware tinkering. 
