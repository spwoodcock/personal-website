# Personal Website

This site is based off of the [eventy-base-blog theme](https://github.com/11ty/eleventy-base-blog), which using eleventy version 3. The site is designed to minimize dependencies and to optimize for speed.

All written content within posts is under the CC0 1.0 license. All icons will have license information within the file itself, (will be either CC0 or MIT) as well as the provider.

Blog RSS Feed: https://spwoodcock.dev/feed.xml
Combined RSS Feed across the web: https://spwoodcock.dev/follow.rss
